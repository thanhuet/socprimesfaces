package viettel.filter;



import viettel.managerBean.loginlogout.UserLogin;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by sm on 7/23/17.
 */
public class LoginFilter implements Filter {

    private static final Set<String> ALLOWED_PATHS = Collections.unmodifiableSet(new HashSet<>(
            Arrays.asList("","/","/login","/login.xhtml", "/logout", "/register")));

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;

        UserLogin userLoginView = (UserLogin)req.getSession().getAttribute("userLogin");
        String path = req.getRequestURI().substring(req.getContextPath().length());

        Boolean loggedIn = userLoginView != null && userLoginView.getLoggedIn();
        Boolean allowedPath = ALLOWED_PATHS.contains(path);

//        Cookie[] cookies=req.getCookies();
//        Cookie code = new Cookie("code", "null");
//        if(cookies!=null && cookies.length > 0){
//            for (int i = 0; i < cookies.length; i++) {
//                if (cookies[i].getName().equals("code")) {
//                    code = cookies[i];
//                    break;
//                }
//            }
//        }
//        Boolean haveCookie =code.getValue().equals("admin");

        if (loggedIn||allowedPath) {
            filterChain.doFilter(servletRequest, servletResponse);
        }else{
            String contextPath = req.getContextPath();
            (res).sendRedirect(contextPath + "/login.xhtml");
        }
    }

    @Override
    public void destroy() {

    }
}
