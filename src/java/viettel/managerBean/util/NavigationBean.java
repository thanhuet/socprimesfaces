package viettel.managerBean.util;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;

/**
 * Created by sm on 7/23/17.
 */
@ManagedBean(name = "navigationBean")
@SessionScoped
public class NavigationBean implements Serializable {
    public  String redirectToLogin(){
        return "/login.xhtml?faces-redirect=true";
    }

    public  String redirectToAdmin(){
        return "/admin.xhtml?faces-redirect=true";
    }

    public String redirectToSignDebtR(){
        return "/sign-debt-reconciliation.xhtml?faces-redirect=true";
    };
}