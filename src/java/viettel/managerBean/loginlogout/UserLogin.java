package viettel.managerBean.loginlogout;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import viettel.managerBean.util.NavigationBean;
import viettel.managerBean.util.SessionUtils;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;

/**
 * Created by sm on 7/20/17.
 */
@ManagedBean(name = "userLogin")
@SessionScoped
public class UserLogin implements Serializable {
    private String username = "";
    private String password = "";
    private Boolean rememberMe = false;
    private Boolean loggedIn = false;
    Cookie code = new Cookie("code", "null");

    @ManagedProperty(value = "#{navigationBean}")
    private NavigationBean navigationBean;

    @PostConstruct
    public void init() {
//        HttpServletRequest request =SessionUtils.getRequest();
//        Cookie[] cookies=request.getCookies();
//        if (cookies != null && cookies.length > 0 ) {
//            for (int i = 0; i < cookies.length; i++) {
//                if (cookies[i].getName().equals("code")) {
//                    code = cookies[i];
//                    break;
//                }
//            }
//        }
//        if(code.getValue().equals("admin")){
//            loggedIn=true;
//            return navigationBean.redirectToAdmin();
//        }
//        else return "login";
    }

    public String login() {
        if (username.trim().equals("admin123") && password.trim().equals("admin123")) {
            HttpSession session = SessionUtils.getSession();
            session.setAttribute("username", "admin");
            loggedIn = true;
            if (rememberMe)
                addCookie(code, username, password);

            return navigationBean.redirectToAdmin();
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Incorrect Username and Passowrd",
                            "Please enter correct username and Password"));
            return null;
        }
    }

    public String logout() {
        loggedIn = false;
        code = new Cookie("code", "null");
        HttpServletResponse response = SessionUtils.getResponse();
        response.addCookie(code);

        HttpSession session = SessionUtils.getSession();
        session.invalidate();
        return navigationBean.redirectToLogin();
    }

    private void addCookie(Cookie code, String username, String password) {
        try {
            Algorithm algorithm = Algorithm.HMAC256("secret");
            String token = JWT.create()
                    .withIssuer("auth0")
                    .sign(algorithm);
            HttpServletResponse response = SessionUtils.getResponse();
            code.setValue(token);
            code.setMaxAge(100);
            response.addCookie(code);
        } catch (UnsupportedEncodingException exception) {
            //UTF-8 encoding not supported
        } catch (JWTCreationException exception) {
            //Invalid Signing configuration / Couldn't convert Claims.
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(Boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    public Boolean getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(Boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public Cookie getCode() {
        return code;
    }

    public void setCode(Cookie code) {
        this.code = code;
    }

    public NavigationBean getNavigationBean() {
        return navigationBean;
    }

    public void setNavigationBean(NavigationBean navigationBean) {
        this.navigationBean = navigationBean;
    }
}
