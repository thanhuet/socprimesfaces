package viettel.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sm on 8/5/17.
 */
@FacesValidator("emailValidator")
public class EmailValidator implements Validator{

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    @Override
    public void validate(FacesContext facesContext, UIComponent uiComponent, Object value) throws ValidatorException {
        String emailValue = value.toString();
        System.out.println("Validating submitted email -- " + emailValue);
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailValue);

        if (!matcher.matches()) {
            FacesMessage msg =
                    new FacesMessage(" E-mail validation failed.",
                            "Please provide E-mail address in this format: abcd@abc.com");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);

            throw new ValidatorException(msg);
        }

    }
}
