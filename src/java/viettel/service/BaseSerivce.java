package viettel.service;

import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

/**
 * Created by sm on 7/24/17.
 */
@Component
abstract class BaseSerivce<T extends Serializable> implements Serializable {

    @PersistenceContext
    private EntityManager entityManager;

    private final Class<T> persistentClass;

    protected BaseSerivce(Class<T> persistentClass){
        this.persistentClass =persistentClass;
//        this.persistentClass =(Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Transactional
    public List<T> findAll(){
        Query query=this.entityManager.createQuery("FROM "+ persistentClass.getName());
        return query.getResultList();
    }

    @Transactional
    public T findById(Integer id){
        return (T)this.entityManager.find(persistentClass,id);
    }

    @Transactional
    public void update(T t){
        this.entityManager.merge(t);
    }

    @Transactional
    public void delete(Integer id){
//        String hql = "DELETE FROM "  +persistentClass.getName()+ " WHERE id = :id";
//        Query query = entityManager.createQuery(hql);
//        query.setParameter("id", id);
//        int result = query.executeUpdate();
        this.entityManager.remove(findById(id));
    }

}
